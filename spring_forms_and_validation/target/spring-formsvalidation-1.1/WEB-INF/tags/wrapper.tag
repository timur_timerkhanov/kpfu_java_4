<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@tag description="Overall Page template" pageEncoding="UTF-8" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="scripts" fragment="true" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><spring:message code="site.title"/></title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<c:url value="../assets/css/base.css"/> ">
    <!-- Custom Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"
          type="text/css">
    <script src="<c:url value="../assets/js/jquery-3.2.0.min.js"/> "></script>
    <script src="<c:url value="../assets/js/bootstrap.min.js"/> "></script>

    <jsp:invoke fragment="scripts"/>
</head>
<body>

<div id="pageheader" class="row">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top">
                    <spring:message code="site.logo"/>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="/posts"><spring:message code="site.title.posts"/></a>
                    </li>
                    <li class="page-scroll">
                        <a href="/"><spring:message code="site.title.calculator"/></a>
                    </li>
                    <li class="page-scroll">
                        <a href="/about"><spring:message code="site.title.about"/></a>
                    </li>
                    <li class="page-scroll">
                        <a href="/register"><spring:message code="site.title.registration"/></a>
                    </li>

                    <li class="page-scroll">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <spring:message code="site.title.language"/>
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="${pageContext.request.contextPath}?mylocale=en">English</a></li>
                                <li> <a href="${pageContext.request.contextPath}?mylocale=ru">Русский</a></li>
                            </ul>
                        </li>


                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->

    </nav>
</div>


<div class="container main">
    <jsp:doBody/>
</div>


<div id="pagefooter">

</div>

<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script>
</body>
</html>