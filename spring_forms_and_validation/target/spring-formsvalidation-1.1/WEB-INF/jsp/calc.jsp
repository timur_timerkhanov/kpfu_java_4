<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper>
    <jsp:attribute name="scripts">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $('#calc').submit(function (e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: "/",
                data: {res: $('#res').val()},
                timeout: 100000,
                success: function (responseBody) {
                    $('#res').val(responseBody)
                }
            })
        });
    });
</script>
    </jsp:attribute>

    <jsp:body>
        <div id="calculator">
            <form class="calc" id="calc" action="/" method="post">

                <div class="top">
                    <button type="button" class="calc-button clear btn" onclick="s('')">C</button>
                    <input type="text" name="res" class="screen" id="res" onfocus="this.blur()">

                </div>
                <div class="keys">
                    <p class="calc-row">
                        <button type="button" class="calc-button btn" onclick="a('7')">7</button>
                        <button type="button" class="calc-button btn" onclick="a('8')">8</button>
                        <button type="button" class="calc-button btn" onclick="a('9')">9</button>
                        <button type="button" class="operator calc-button btn" onclick="a('*')">x</button>
                    </p>
                    <p class="calc-row">
                        <button type="button" class="calc-button btn" onclick="a('4')">4</button>
                        <button type="button" class="calc-button btn" onclick="a('5')">5</button>
                        <button type="button" class="calc-button btn" onclick="a('6')">6</button>
                        <button type="button" class="operator calc-button btn" onclick="a('-')">-</button>
                    </p>
                    <p class="calc-row">
                        <button type="button" class="calc-button btn" onclick="a('1')">1</button>
                        <button type="button" class="calc-button btn" onclick="a('2')">2</button>
                        <button type="button" class="calc-button btn" onclick="a('3')">3</button>
                        <button type="button" class="operator calc-button btn" onclick="a('+')">+</button>
                    </p>
                    <p class="calc-row">
                        <button type="button" class="calc-button btn" onclick="a('0')">0</button>
                        <button type="button" class="calc-button btn" onclick="a('.')">.</button>
                        <input type="submit" id="eval" value="=" name="eval" class="calc-button btn operator eval">
                        <button type="button" class="operator calc-button btn" onclick="a('/')">/</button>

                    </p>
                </div>
            </form>
        </div>
        <script>
            function s(v) {
                document.getElementById('res').value = v
            }
            function a(v) {
                document.getElementById('res').value += v
            }
            //    function e() {
            //        try {
            //            s(eval(document.getElementById('res').value))
            //        } catch(e) { s('Error') }
            //    }
        </script>
    </jsp:body>

</t:wrapper>




