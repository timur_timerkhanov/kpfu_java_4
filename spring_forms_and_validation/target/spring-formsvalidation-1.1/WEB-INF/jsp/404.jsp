<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTMl>
<html>
<head>
    <title>404</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="<c:url value="../assets/css/404style.css"/>"
          type="text/css" media="all"/>
    <link href='http://fonts.googleapis.com/css?family=Strait' rel='stylesheet' type='text/css'>
</head>
<body>

<div class="wrap">
    <h1>Company Name</h1>
    <div class="banner"><img src="<c:url value="../assets/images/fuel-404-logo.png"/>"
                             alt=""/></div>
    <div class="text-center search">
        <a href="/posts">Back to App</a>
    </div>

</div>

</body>
</html>    

