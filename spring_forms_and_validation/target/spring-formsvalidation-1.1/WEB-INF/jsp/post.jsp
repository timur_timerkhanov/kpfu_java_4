<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper>
    <jsp:body>
        <div class="container">
        <div class="row">
            <!-- Blog Post Content Column -->
            <div class="col-lg-12">

                <!-- Blog Post -->
                <!-- Title -->
                <h1>${post.title}</h1>
                <!-- Author -->
                <p class="lead">
                    by <a href="#">Admin</a>
                </p>
                <hr>
                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted ${post.pubDate}</p>

                <hr>
                <!-- Post Content -->
                ${post.text}
                <hr>
            </div>
            <!-- /.row -->
            <hr>
        </div>
    </jsp:body>
</t:wrapper>