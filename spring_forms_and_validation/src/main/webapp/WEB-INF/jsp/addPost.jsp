<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:wrapper>
    <jsp:attribute name="scripts">

        <!-- include libraries(jQuery, bootstrap) -->
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <form method="post" name="Form" onsubmit="return validateForm()">
                        <div class="form-group">
                            <label for="title"><spring:message code="posts.title"/></label>
                            <input type="text" name="title" class="form-control" id="title">
                        </div>
                        <div class="form-group">
                            <label for="editor"><spring:message code="posts.text_ru"/>:</label>

                            <textarea class="form-control" id="editor" name="text_ru" rows="15">
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label for="editor1"><spring:message code="posts.text_en"/>:</label>

                            <textarea class="form-control" id="editor1" name="text_en" rows="15">
                            </textarea>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">

                                <spring:message code="posts.save"/>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>


        <script>
            $(document).ready(function() {
                $('#editor').summernote(
                    {

                        width: '100%',
                        height: 400,
                    }
                );

                $('#editor1').summernote(
                    {

                        width: '100%',
                        height: 400,
                    }
                );
            });
        </script>


        <script type="text/javascript">
            function validateForm()
            {
                var a=document.forms["Form"]["title"].value;
                if (a==null || a=="")
                {
                    alert("Please Fill All Required Field");
                    return false;
                }
            }
        </script>



    </jsp:body>
</t:wrapper>
