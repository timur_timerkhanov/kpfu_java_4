<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

down vote
accepted
You can use the JSTL substring function:

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:wrapper>
    <jsp:body>
        <div class="container">
            <div class="row">


                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9">
                            <h3><spring:message code="posts.header"/></h3>
                        </div>


                        <div class="col-md-3" style="text-align:right">
                            <button class="btn btn-primary"
                                    style="border-color: transparent;
                                background-color: #1976D2">
                                <a style="text-decoration: none; color: white;"
                                   href="<c:url value="/posts/add"/>">
                                    <spring:message code="posts.button.add"/></a>
                            </button>
                        </div>
                    </div>


                    <div class="spacing"></div>

                    <form class="form-inline">
                        <div class="form-group col-md-2">
                            <h6><spring:message code="posts.title"/></h6>
                        </div>
                        <div class="form-group col-md-5">
                            <h6><spring:message code="posts.text"/></h6>
                        </div>
                        <div class="form-group col-md-1">
                            <h6><spring:message code="posts.author"/></h6>
                        </div>
                        <div class="form-group col-md-2">
                            <h6><spring:message code="posts.pub_date"/></h6>
                        </div>
                        <div class="form-group col-md-1">
                            <h6><spring:message code="posts.edit"/></h6>
                        </div>
                        <div class="form-group col-md-1">
                            <h6><spring:message code="posts.delete"/></h6>
                        </div>
                    </form>


                    <c:forEach items="${posts}" var="post">

                        <div class="row">

                            <form class="form-inline" method="post">
                                <div class="form-group col-md-2">
                                    <a href="<c:url value="/posts/${post.slug}"/> ">${post.title}</a>
                                </div>
                                <div class="form-group col-md-5">
                                    <c:out value="${fn:substring(post.text, 0, 300)} ..."/>

                                </div>
                                <div class="form-group col-md-1">
                                    Admin
                                </div>
                                <div class="form-group col-md-2">
                                        ${post.pubDate}
                                </div>
                                <input type="hidden" name="slug" value="${post.slug}" id="">
                                <div class="form-group col-md-1">
                                    <button class="btn btn-primary btn-md"
                                            name="edit"
                                            value="edit">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </button>
                                </div>
                                <div class="form-group col-md-1">
                                    <button class="btn btn-danger btn-md" data-title="Delete"
                                            type="submit" value="delete"
                                            name="delete">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </div>

                            </form>
                        </div>
                        <hr>

                    </c:forEach>


                </div>
            </div>
        </div>


        </div>
    </jsp:body>
</t:wrapper>