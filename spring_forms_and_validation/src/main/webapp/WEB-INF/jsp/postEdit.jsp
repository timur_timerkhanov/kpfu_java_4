<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page trimDirectiveWhitespaces="true" %>

<t:wrapper>
    <jsp:attribute name="scripts">
        <!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>
    </jsp:attribute>


    <jsp:body>
        <div class="container">
        <div class="row">
            <!-- Blog Post Content Column -->
            <div class="col-lg-12">

                <!-- Blog Post -->
                <!-- Title -->
                <form method="post">
                    <h1>
                        <input type="text" value="${post.title}" name="title">
                    </h1>
                    <!-- Author -->
                    <p class="lead">
                        by <a href="#">Admin</a>
                    </p>
                    <hr>
                    <!-- Date/Time -->
                    <p>
                        <span class="glyphicon glyphicon-time">
                        </span>
                        <spring:message code="posts.posted"/> ${post.pubDate}
                    </p>

                    <hr>
                    <!-- Post Content -->
                    <textarea name="text_ru" id="editor" style="width:100%" rows="20 ">
                            ${fn:trim(post.textRu)}
                    </textarea>

                    <textarea name="text_en" id="editor1" style="width:100%" rows="20 ">
                            ${fn:trim(post.textEn)}
                    </textarea>
                    <div class="spacing"></div>

                    <div class="text-center">
                        <input type="submit" class="btn btn-primary"
                               value="<spring:message code="posts.edit"/>">
                    </div>

                    <hr>
                </form>
            </div>

            <!-- /.row -->
            <hr>
        </div>


        <script>
            $(document).ready(function () {
                $('#editor').summernote(
                    {

                        width: '100%',
                        height: 400,
                    }
                );

                $('#editor1').summernote(
                    {

                        width: '100%',
                        height: 400,
                    }
                );
            });
        </script>

    </jsp:body>
</t:wrapper>