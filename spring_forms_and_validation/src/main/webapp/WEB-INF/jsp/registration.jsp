<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:wrapper>
    <jsp:body>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading" id="my_panel_head">
                        <h3 class="panel-title">Регистрация</h3>
                        <a href="register?mylocale=en">English </a> | <a href="register?mylocale=ru">Русский </a>
                    </div>


                    <%--<c:if test="${not empty message}">--%>
                        <%--<h1>${message}</h1>--%>
                    <%--</c:if>--%>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1" style="border-left:1px solid #EEEEEE;">
                                <form:form method="post" commandName="user" cssClass="registrationForm">
                                    <spring:bind path="email">
                                        <div class="form-group spacing">
                                            <form:label path="email" cssClass="col-md-3 control-label">
                                                <spring:message code="registration.email"/>
                                            </form:label>
                                            <div class="col-md-8">
                                                <form:input path="email" type="text" cssClass="form-control"/>
                                            </div>
                                            <br>

                                        </div>
                                        <div class="errors">
                                            <form:errors path="email"/>
                                        </div>
                                    </spring:bind>

                                    <div class="spacing"></div>

                                    <div class="form-group spacing">
                                        <form:label path="password" cssClass="col-md-3 control-label">
                                            <spring:message code="registration.password"/>
                                        </form:label>
                                        <div class="col-md-8">
                                            <form:input path="password" type="password" cssClass="form-control"/>
                                        </div>
                                        <br>

                                    </div>
                                    <div class="errors">
                                        <form:errors path="password"/>
                                    </div>
                                    <div class="spacing"></div>

                                    <div class="form-group spacing">
                                        <form:label path="name" cssClass="col-md-3 control-label">
                                            <spring:message code="registration.full_name"/>
                                        </form:label>
                                        <div class="col-md-8">
                                            <form:input path="name" type="text" cssClass="form-control"/>
                                        </div>
                                        <br>

                                    </div>
                                    <div class="errors">
                                        <form:errors path="name"/>
                                    </div>
                                    <div class="spacing"></div>

                                    <div class="form-group spacing">
                                        <form:label path="sex" cssClass="col-md-3 control-label">
                                            <spring:message code="registration.sex"/>
                                        </form:label>
                                        <div class="col-md-8">
                                            <form:select path="sex" cssClass="form-control" items="${sexList}"/>
                                        </div>
                                    </div>
                                    <br>
                                    <form:errors path="sex"/>

                                    <div class="spacing"></div>

                                    <div class="form-group spacing">
                                        <form:label path="country" cssClass="col-md-3 control-label">
                                            <spring:message code="registration.country"/>
                                        </form:label>
                                        <div class="col-md-8">
                                            <form:select path="country" cssClass="form-control" items="${countryList}"/>
                                        </div>
                                        <br>
                                        <form:errors path="country"/>
                                    </div>

                                    <div class="spacing"></div>
                                    <div class="form-group spacing">
                                        <form:label path="subscribed" cssClass="col-md-3 control-label">
                                            <spring:message code="registration.subscribe"/>
                                        </form:label>
                                        <div class="col-md-8">
                                            <form:checkbox path="subscribed" value="Yes"/>
                                        </div>
                                    </div>
                                    <div class="spacing"></div>
                                    <div class="spacing"></div>

                                    <div class="form-group">
                                        <div class="text-center">
                                            <input type="submit" value="<spring:message code="registration.register"/>" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </jsp:body>
</t:wrapper>