<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper>
    <jsp:body><p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Ad expedita officiis quis, saepe totam veritatis!
        Accusamus animi aperiam consectetur dolor dolore,
        dolorem dolorum ducimus eos eveniet ex explicabo
        illo magni maiores necessitatibus neque nihil officia
        officiis placeat porro praesentium quaerat qui quibusdam quo quod
        recusandae reiciendis repudiandae rerum tenetur unde voluptas? Cum magni nisi unde.
        Atque aut consectetur corporis cumque dolorem explicabo libero molestiae molestias
        nisi odit optio, pariatur, sed voluptatem?
        Ad asperiores maxime molestiae officia repellat!
        Aliquid architecto at beatae, cumque deserunt eligendi illo ipsa
        ipsum labore quasi quisquam, quos reiciendis repudiandae sequi tempora!
        Cum distinctio fugit recusandae sapiente.
    </p>
    </jsp:body>

</t:wrapper>