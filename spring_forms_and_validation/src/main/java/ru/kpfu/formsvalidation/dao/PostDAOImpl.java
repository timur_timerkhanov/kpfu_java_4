package ru.kpfu.formsvalidation.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.kpfu.formsvalidation.model.Post;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class PostDAOImpl implements PostDAO {

    public PostDAOImpl() {}

    private JdbcTemplate jdbcTemplate;
    @Autowired
    public PostDAOImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void saveOrUpdate(Post post) {
        //Update
        if (post.getId() > 0) {
            String sql = "UPDATE post SET title=?, text_ru=?," +
                    "text_en = ?, publication_date=?, slug=? WHERE id=?";
            jdbcTemplate.update(sql, post.getTitle(), post.getTextRu(),
                    post.getTextEn(), post.getPubDate(),
                    post.getSlug(), post.getId());
        }
        // Insert
        else {
            String sql = "INSERT INTO post (title, text_ru, text_en, publication_date, slug)" +
                    "VALUES (?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, post.getTitle(), post.getTextRu(),
                    post.getTextEn(), post.getPubDate(), post.getSlug());
        }

    }

    @Override
    public Post get(String slug) {
        String sql = "SELECT * FROM post WHERE slug='" + slug + "'";
        return jdbcTemplate.query(sql, new ResultSetExtractor<Post>() {
            @Override
            public Post extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if (resultSet.next()) {
                    Post post = new Post();
                    post.setId(resultSet.getInt("id"));
                    post.setTitle(resultSet.getString("title"));
                    post.setTextEn(resultSet.getString("text_en"));
                    post.setTextRu(resultSet.getString("text_ru"));
                    post.setPubDate(resultSet.getTimestamp("publication_date"));
                    post.setSlug(resultSet.getString("slug"));
                    return post;
                }
                return null;
            }
        });
    }

    @Override
    public void delete(String slug) {
        String sql = "DELETE FROM post WHERE slug = ?";
        jdbcTemplate.update(sql, slug);
    }

    @Override
    public List<Post> getAll() {
        String sql = "SELECT * FROM post ORDER BY publication_date";
        List<Post> posts = jdbcTemplate.query(sql, new RowMapper<Post>() {
            @Override
            public Post mapRow(ResultSet resultSet, int i) throws SQLException {
                Post aPost = new Post();

                aPost.setId(resultSet.getInt("id"));
                aPost.setTitle(resultSet.getString("title"));
                aPost.setTextEn(resultSet.getString("text_en"));
                aPost.setTextRu(resultSet.getString("text_ru"));
                aPost.setSlug(resultSet.getString("slug"));
                aPost.setPubDate(resultSet.getTimestamp("publication_date"));
                return aPost;
            }
        });
        return posts;
    }

}
