package ru.kpfu.formsvalidation.dao;

import ru.kpfu.formsvalidation.model.Post;

import java.util.List;

public interface PostDAO {
    void saveOrUpdate(Post post);
    Post get(String slug);
    void delete(String slug);
    List<Post> getAll();
}
