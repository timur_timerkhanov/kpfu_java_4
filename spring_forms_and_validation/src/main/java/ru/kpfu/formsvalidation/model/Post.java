package ru.kpfu.formsvalidation.model;

import java.sql.Timestamp;

public class Post {
    private int id;

    private String title;
    private String text;
    private Timestamp pubDate;
    private String slug;
    private String textRu;
    private String textEn;

    public Post(){
    }

    public Post(int id, String title,
                String slug, String textRu, String textEn) {
        this.id = id;
        this.title = title;
        this.slug = slug;
        this.textRu = textRu;
        this.textEn = textEn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getPubDate() {
        return pubDate;
    }

    public void setPubDate(Timestamp pubDate) {
        this.pubDate = pubDate;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTextRu() {
        return textRu;
    }

    public void setTextRu(String textRu) {
        this.textRu = textRu;
    }

    public String getTextEn() {
        return textEn;
    }

    public void setTextEn(String textEn) {
        this.textEn = textEn;
    }
}
