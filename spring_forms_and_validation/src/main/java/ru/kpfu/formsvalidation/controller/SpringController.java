package ru.kpfu.formsvalidation.controller;

import com.github.slugify.Slugify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.formsvalidation.dao.PostDAO;
import ru.kpfu.formsvalidation.model.Post;
import ru.kpfu.formsvalidation.model.User;
import ru.kpfu.formsvalidation.model.UserValidator;
import ru.kpfu.formsvalidation.service.EvalService;
import ru.kpfu.formsvalidation.service.LanguageResolverService;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Controller
public class SpringController {
    //    private final Logger logger = LoggerFactory.getLogger(SpringController.class);
    @Autowired
    UserValidator userValidator;

    @Autowired
    LanguageResolverService languageResolverService;

    @Autowired
    PostDAO postDAO;

    private final EvalService evalService;

    @InitBinder("user")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(userValidator);
    }

    @Autowired
    public SpringController(EvalService evalService) {
        this.evalService = evalService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String calculateGET() {
        return "calc";
    }

    @ResponseBody
    @RequestMapping(value = {"/"}, method = RequestMethod.POST)
    public String calculate(@RequestParam String res) throws IOException {
        String res1 = String.valueOf(evalService.eval(res));
        return res1;
    }

    @RequestMapping(value = {"/about"})
    public String about() {
        return "about";
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String allPosts(ModelMap model) throws IOException {
        try {
            Locale locale = LocaleContextHolder.getLocale();

            List<Post> posts = languageResolverService.getPostByLanguage(locale);

            model.put("posts", posts);
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
        }
        return "allPosts";
    }

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public String deletePost(@RequestParam Map<String, String> allRequestParams) {
        String result = "";
        if (allRequestParams.get("delete") != null) {
            postDAO.delete(allRequestParams.get("slug"));
            result = "redirect:/posts";
        } else if (allRequestParams.get("edit") != null) {
            result = "redirect:/posts/edit/" + allRequestParams.get("slug");
        }
        return result;
    }

    @RequestMapping(value = "/posts/edit/{slug}", method = RequestMethod.GET)
    public String editPostGET(@PathVariable String slug, ModelMap modelMap) {
        Post post = postDAO.get(slug);
        if (post != null) {
            modelMap.put("post", post);
            return "postEdit";
        } else
            return "404";
    }

    @RequestMapping(value = "/posts/edit/{slug}", method = RequestMethod.POST)
    public String editPostPOST(@PathVariable String slug,
                               @RequestParam Map<String, String> allRequestParams) {
        Post post = postDAO.get(slug);
        post.setTitle(allRequestParams.get("title"));
        post.setTextRu(allRequestParams.get("text_ru"));
        post.setTextEn(allRequestParams.get("text_en"));

        Slugify slg = new Slugify();
        String result = slg.slugify(allRequestParams.get("title"));

        post.setSlug(result);

        postDAO.saveOrUpdate(post);
//        Post post = postDAO.saveOrUpdate(slug);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/posts/add", method = RequestMethod.GET)
    public String newPost() {
        return "addPost";
    }

    @RequestMapping(value = "/posts/add", method = RequestMethod.POST)
    public String addPost(@RequestParam Map<String, String> allRequestParams, ModelMap modelMap) {

        Post post = new Post();
        post.setTitle(allRequestParams.get("title"));

        post.setTextEn(allRequestParams.get("text_en"));
        post.setTextRu(allRequestParams.get("text_ru"));

        Slugify slg = new Slugify();
        String result = slg.slugify(allRequestParams.get("title"));
        post.setSlug(result);
        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
        post.setPubDate(currentTimestamp);
        try {
            postDAO.saveOrUpdate(post);
        } catch (DuplicateKeyException e) {
            int min = 1;
            int max = 9;
            int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
            post.setSlug(result + String.valueOf(randomNum));
            postDAO.saveOrUpdate(post);
        }

        return "redirect:/posts";
    }

    @RequestMapping(value = "/posts/{slug}", method = RequestMethod.GET)
    public String showPost(@PathVariable String slug, ModelMap modelMap) {

        Locale ruLocale = new Locale("ru");
        Locale enLocale = new Locale("en");
        Locale locale = LocaleContextHolder.getLocale();

        Post post = postDAO.get(slug);
        if (locale.equals(ruLocale)) {
            post.setText(post.getTextRu());
        } else {
            if (locale.equals(enLocale)) {
                post.setText(post.getTextEn());
            }
        }

        if (post != null) {
            modelMap.put("post", post);
            return "post";
        } else
            return "404";
    }


    // Registration

    @RequestMapping(value = {"/register"}, method = RequestMethod.GET)
    public String register(Locale locale, ModelMap modelMap) {
        modelMap.put("user", new User());
        populateDefaultModel(modelMap);
        return "registration";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String saveOrUpdateUser(
            RedirectAttributes redirectAttributes,
            @Validated @ModelAttribute("user") User user,
            BindingResult result,
            ModelMap model) {

        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "registration";
        } else {
            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            redirectAttributes.addFlashAttribute("msg", "User added successfully!");
            return "redirect:/success/";
        }
    }


    private void populateDefaultModel(ModelMap modelMap) {
        Map<String, String> sex = new HashMap<>();
        sex.put("M", "Male");
        sex.put("F", "Female");
        modelMap.put("sexList", sex);

        Map<String, String> country = new HashMap<>();
        country.put("US", "United Stated");
        country.put("CHINA", "China");
        country.put("RU", "Russia");
        country.put("IT", "Italy");
        modelMap.put("countryList", country);
    }
}

