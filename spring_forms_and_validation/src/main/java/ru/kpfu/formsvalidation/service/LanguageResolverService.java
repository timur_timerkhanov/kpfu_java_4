package ru.kpfu.formsvalidation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.formsvalidation.dao.PostDAO;
import ru.kpfu.formsvalidation.model.Post;

import java.util.List;
import java.util.Locale;

@Service
public class LanguageResolverService {
    @Autowired
    private PostDAO postDAO;



    Locale ruLocale = new Locale("ru");
    Locale enLocale = new Locale("en");

    public List<Post> getPostByLanguage(Locale locale) {
        List<Post> posts = postDAO.getAll();

        if (ruLocale.equals(locale)) {
            for (Post post : posts) {
                post.setText(post.getTextRu());
            }
        } else if (enLocale.equals(locale)) {
            for (Post post : posts) {
                post.setText(post.getTextEn());
            }
        }
        return posts;
    }
}
